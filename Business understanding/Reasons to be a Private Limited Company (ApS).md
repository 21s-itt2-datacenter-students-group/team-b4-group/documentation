Reasons to be a Private Limited Company (ApS)

1.	Significantly lessened personal liability 

The biggest pro to forming an actual company, is the legal safety net it brings. To put it simply, if the company should run into trouble, your personal assets would be secure. This is because a limited company is treated as a separate legal entity, making it separate from those wo own and manage it.

2.	Tax efficiency

As a company is Denmark, taxes can come down to as low as 20% against an Entrepreneur’s 44%. If the company were started in the UK, it could come down to 19%.
also, rather than withdrawing all available profits each year and paying more personal tax on top of your Corporation Tax liability, you can retain surplus income in the business to pay for future operational costs and growth. This makes more sense than withdrawing all profits, paying higher rates of Income Tax, and reinvesting your own finances when the business needs additional capital.
By setting up a company, you can reduce your Income Tax by taking a combination of a salary and dividends. If you keep your director’s salary below the national Threshold, you will not have to pay any Income Tax on those earnings. Furthermore, the company will not pay Corporation Tax on the salary, because wages are a deductible business expense.

3.	Investments

Companies can have multiple owners, so it is possible to raise additional capital by selling portions (‘shares’) in the business to new investors

4.	Spread out the workload

As an Entrepreneur, the workload could become practically unbearable, whereas an ApS would have employees available to spread out the work, thus increasing work efficiency 
