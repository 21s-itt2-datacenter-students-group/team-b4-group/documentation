# Summary of test2

Second test will include the AMS CMS811 air quality sensor. And the test is to get a functional sensor, giving the users outputs of air quality, that can later be implemented into a dashboard envireoment.

# Hardware

The components used for this test are the following:

* 1 RPI 4
* Arduino
* 6 jumper wires
* 1 pmod AQS CMS811 sensor
# Wiring

The wires should be conneted the following way:

* RPI (arduino), 3v3 = CMS, VCC
* RPI (arduino), GROUND = CMS, GROUND
* RPI (arduino), SCL = CMS, SCL
* RPI (arduino), SDA = CMS, SDA
* RPI (arduino), GROUND = CMS, WAKE
* RPI (arduino), GROUND = CMS, INT

# Test code

```
Test code was downloaded from from digilents website. Can be found under links.

/***************************************************************************
  This is a library for the PmodAQS air 

  This sketch reads the PmodAQS sensor

  These sensors use I2C to communicate. The device's I2C address is 0x5B

  
  Written by Dean Miller for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
  
  updated to work with the PmodAQS
 ***************************************************************************/

#include "PmodAQS.h"

PmodAQS ccs;

void setup() {
  Serial.begin(9600);
  
  Serial.println("PmodAQS test");
  
  if(!ccs.begin(0x5B)){
    Serial.println("Failed to start sensor! Please check your wiring.");
    while(1);
  }

  //calibrate temperature sensor
  while(!ccs.available());
  float temp = ccs.calculateTemperature();
  ccs.setTempOffset(temp - 25.0);
}

void loop() {


  if(ccs.available()){
       
    float temp = ccs.calculateTemperature();
	
    if(ccs.readData()){
      Serial.print("CO2: ");
      Serial.print(ccs.geteCO2());
      Serial.print("ppm, TVOC: ");
      Serial.print(ccs.getTVOC());
      Serial.println("ppb");
    }
    else{
	  Serial.println("ERROR!");
      while(1);
    }
  }
  delay(500);
}
```

# Results

Programming a test code for RPI and pmod AQS proved to be rather complex. But it also proved to be possible with the python libary: SMBus2. 
Tho it requires a bit more knowledge than teammembers have atm. 
Therefor it was also tested with arduino, where code was allready provided. It proved to be a success. Even testing with serial connection with RPI aswell, succesfully trasnfering data from arduino to RPI. 


# Links

* https://reference.digilentinc.com/reference/pmod/pmodaqs/start
- Credit to digilent, for providing datasheet, component instructions and test code in the above link
