# Summary of test3
TBH
# Hardware

The components used for this test are the following:

* 1 RPI 4
* 4 jumper wires
* 2 x 10k resistor
* 1 sensorian 810 -125pa diffrential sensor

# Wiring

The wires should be conneted the following way:

* RPI, 3v3 = sensorian, VCC
* RPI, GROUND = sensorian, GROUND
* RPI, pin3, SCL = sensorian, SCL
* RPI, pin5,  SDA = sensorian, SDA

# Test code
TBH
# Results
TBH


# Links
TBH
