'''
Inspiration from http://www.steves-internet-guide.com/into-mqtt-python-client/
created by: Nikolaj Simonsen | february 2021
'''
import paho.mqtt.client as mqtt
import time
from datetime import datetime
import json
import Adafruit_DHT
import serial
import urllib.request
import socket
import RPi.GPIO as GPIO
import smbus

#intiate variables for leds
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
redled = 23
blueled = 24
GPIO.setup(redled ,GPIO.OUT)
GPIO.setup(blueled ,GPIO.OUT)

# variables for mqqt and broker
sensor_id = 1234
client_name = 'publisher client'
broker_name = 'broker.hivemq.com' #warning
broker_port = 1883
topic1 = 'rackroom/sensor'
publish_interval = 10
client = mqtt.Client(client_name)  # init clien

#variables for dht11
DHT_SENSOR = Adafruit_DHT.DHT11
DHT_PIN = 4

#variables for sensorian and stops cont measurements
bus=smbus.SMBus(1) #The default i2c bus
address=0x25

# opening serial for arduini data, so the reading is continues
try:
    ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)

except:
    print("Check serial connection!")

# received message callback
def on_message(client, userdata, message):
    print('message received ', str(message.payload.decode('utf-8')))
    # print('message topic=',message.topic)
    # print('message qos=',message.qos)
    # print('message retain flag=',message.retain)


client.on_message = on_message  # attach message callback to callback

def connect(host='http://google.com'):
    try:
        urllib.request.urlopen(host)
        onlineMode= 0
        return onlineMode
    except:
        onlineMode= 1
        return onlineMode

# create payload
def build_payload(sensor_id, onlineMode, sensor_read, AQS_read, real_preasure):
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # utc timestamp
    faultchecker = 0
    #onlineMode=connect()
    GPIO.output(redled, False)
    GPIO.output(blueled, False)
    
    if onlineMode == 1:
        print("No internet connection!")
        GPIO.output(redled, True)
        
    if sensor_read == "" or AQS_read == "" or real_preasure == "":
        print("Fault in code, check wiring. Publishing anyway")
        GPIO.output(blueled, True)
        faultchecker = 1
        
    payload = {'faulterror': faultchecker, 'sensor_time': timestamp, 'temp_value': sensor_read[0:5], 'hum_value': sensor_read[7:12], 'Co2_value': AQS_read[4:8], 'Tvoc_value': AQS_read[13:16], 'Differential_preassure': real_preasure}
    return json.dumps(payload)  # convert to json format


# read dht11
def read_sensor():

    temperature, humidity = Adafruit_DHT.read(DHT_SENSOR, DHT_PIN)

    if humidity is not None and temperature is not None:

        sensor_read = "{0:0.1f}C, {1:0.1f}%".format(temperature, humidity)
        return sensor_read

    else:
        sensor_read = ""
        return sensor_read
 
        
def arduino_ccs811():
    
   
    try:
        

        while True: 
            if(ser.in_waiting >0):
                AQS_read = ser.readline().decode('utf-8').rstrip()
                return AQS_read
    except:
        AQS_read = ""
        return AQS_read
    
def sensorian():
    
    try:
        bus.write_i2c_block_data(address, 0x3F, [0xF9])
        time.sleep(0.8)
        bus.write_i2c_block_data(address, 0x36, [0X03]) # The command code 0x3603 is split into two arguments, cmd=0x36 and [val]=0x03
        #print ("Taking 5 readings of 9 bites each. See table in section 5.3.1 of datasheet for meaning of each bite")
        for x in range (0, 5):    
            time.sleep(2)
            reading=bus.read_i2c_block_data(address,0,9)
            #print (reading)
            
        for x in range (0, 1):    
            time.sleep(2)
            reading=bus.read_i2c_block_data(address,0,9)
            pressure_value=reading[0]+float(reading[1])/255
            if pressure_value>=0 and pressure_value<128:
                diffirential_pressure=pressure_value*240/256 #scale factor adjustment
            elif pressure_value>128 and pressure_value<=256:
                diffirential_pressure=-(256-pressure_value)*240/256 #scale factor adjustment
            elif pressure_value==128:
                diffirential_pressure=99999999 #Out of range
            
            return diffirential_pressure
    except:
        AQS_read = ""
        return AQS_read

# connecting to broker

try:
    print('connecting to broker')
    client.will_set("rackroom/sensor", payload=json.dumps('Offline'), qos=0, retain=True)
    client.connect(broker_name, broker_port)
    print(f'connected to {broker_name} on port {broker_port} ')


except socket.error as e:
    print(f'could not connect {client_name} to {broker_name} on port {broker_port}\n {e}')
    exit(0)

# subscribe
client.loop_start()  # start the loop
client.subscribe(topic1)
print(f'Subscribed to topics: {topic1}')

# publish
while (True):
    try:
        payload = build_payload(sensor_id, connect(), read_sensor(), arduino_ccs811(), sensorian())
        if payload == None:
            pass
        else:
            print(f'Publishing {payload} to topic, {topic1}')
            client.publish(topic1, payload)
            time.sleep(publish_interval)  # wait x seconds

    except KeyboardInterrupt:  # cleanup nice
        client.loop_stop()
        GPIO.output(redled, False)
        GPIO.output(blueled, False)# stop the loop
        GPIO.cleanup()
        exit(0)

