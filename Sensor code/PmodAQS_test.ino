/***************************************************************************
  This is a library for the PmodAQS air 

  This sketch reads the PmodAQS sensor

  These sensors use I2C to communicate. The device's I2C address is 0x5B

  
  Written by Dean Miller for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
  
  updated to work with the PmodAQS
 ***************************************************************************/

#include "PmodAQS.h"

PmodAQS ccs;

void setup() {
  Serial.begin(9600);
  
  if(!ccs.begin(0x5B)){
    Serial.println("Failed to start sensor! Please check your wiring.");
    while(1);
  }

  //calibrate temperature sensor
  while(!ccs.available());
  float temp = ccs.calculateTemperature();
  ccs.setTempOffset(temp - 25.0);
}

void loop() {


  if(ccs.available()){
       
    float temp = ccs.calculateTemperature();
	
    if(ccs.readData()){
      //Serial.print("CO2: ");
      Serial.print(ccs.geteCO2());
      Serial.print(" ");
      Serial.print(ccs.getTVOC());
    
    }
    else{
	  Serial.println("ERROR!");
      while(1);
    }
  }
  delay(2500);
}
